using Toybox.System;
import Toybox.Time;
import Toybox.Lang;

(:background)
class FormatHelper {
function printCurrentTime() as Void {
        var timeInfo = Time.Gregorian.info(Time.now(), Time.FORMAT_SHORT);
        var timestamp = Lang.format(
            "$1$-$2$-$3$ $4$:$5$:$6$",
            [timeInfo.day.format("%02d"), timeInfo.month.format("%02d"), timeInfo.year.format("%4d"),
            timeInfo.hour.format("%02d"), timeInfo.min.format("%02d"), timeInfo.sec.format("%02d")]
        );
        System.println(timestamp);
    }
}