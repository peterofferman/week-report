import Toybox.Lang;
import Toybox.Graphics;

class BitmapSettings {
    var bitmap as BitmapReference;
    var x as Number;
    var y as Number;

    function initialize(bitmap as BitmapReference, x as Number, y as Number)
    {
        self.bitmap = bitmap;
        self.x = x;
        self.y = y;
    }
}