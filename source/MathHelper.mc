import Toybox.Lang;

(:background)
class MathHelper {
    static function max(a as Numeric, b as Numeric)
    {
        if(a > b)
        {
            return a;
        }
        return b;
    }
}