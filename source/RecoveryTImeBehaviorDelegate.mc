import Toybox.Application;
import Toybox.WatchUi;
class RecoveryTimeBehaviorDelegate extends BehaviorDelegate {
    function initialize() {
        BehaviorDelegate.initialize();
    }
    
    function onPreviousPage() {
        System.println("onPreviousPage");
        WatchUi.popView(WatchUi.SLIDE_DOWN);
        return true;
    }
}