import Toybox.Lang;

(:glance)
class ChartPoint {
    var daysSinceStart as Float;
    var seconds as Number;

    function initialize(daysSinceStart as Float, seconds as Number)
    {
        self.daysSinceStart = daysSinceStart;
        self.seconds = seconds;
    }
}