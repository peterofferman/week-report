import Toybox.Graphics;
import Toybox.Lang;
import Toybox.Time;

(:glance)
class BarChart extends Chart {
    function initialize(canvas as Rectangle, barWidth as Number, padding as Number, margin as Number) {
        Chart.initialize(canvas, barWidth, padding, margin);
    }

    function draw(dc as Dc, firstDay as Time.Moment, chartPoints as Array<ChartPoint>) {
        var maxDuration = MathHelper.max(60, getMaxValue(chartPoints));      
        
        var maxDurationText = "";
        var multiplier = Gregorian.SECONDS_PER_MINUTE;
        var durationAppendix = Rez.Strings.MinutesShort;        
        if(maxDuration > Gregorian.SECONDS_PER_HOUR / 2)
        {
            multiplier = Gregorian.SECONDS_PER_HOUR;
            durationAppendix = Rez.Strings.HoursShort;
        }
        var labelMaxDuration = maxDuration.toFloat() / multiplier.toFloat();            
        if(labelMaxDuration < 9.95)
        {
            labelMaxDuration = Math.round(labelMaxDuration * 2) / 2;
            if(labelMaxDuration - labelMaxDuration.toNumber() > 0)
            {
                maxDurationText = labelMaxDuration.format("%2.1f");    
                if(maxDurationText.equals("0.5") && durationAppendix == Rez.Strings.HoursShort)
                {
                    maxDurationText = "30";
                    durationAppendix = Rez.Strings.MinutesShort;
                }
            }else
            {
                maxDurationText = labelMaxDuration.format("%2d");
            }            

        }else
        {
            labelMaxDuration = Math.round(labelMaxDuration / 5) * 5;
            maxDurationText = labelMaxDuration.format("%2d");
        }
                
        labelMaxDuration = labelMaxDuration * multiplier;        

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        //drawAxes(dc);
        drawLegend(dc, maxDurationText + WatchUi.loadResource(durationAppendix).toString(), maxDuration, labelMaxDuration);

        for(var i = 0; i < chartPoints.size(); i++)
        {            
            var dayInfo = Gregorian.info(firstDay.add(Gregorian.duration({:days => i})), Time.FORMAT_MEDIUM);
            drawDayText(dc, i, dayInfo.day_of_week.substring(0, 1));
            if(maxDuration > 0)
            {
               drawBar(dc, i, chartPoints[i].seconds, MathHelper.max(maxDuration, labelMaxDuration));      
            }            
        }
        //dc.drawRectangle(chartSettings.canvas.x, chartSettings.canvas.y, chartSettings.canvas.width, chartSettings.canvas.height);
    }

    function drawBar(dc as Dc, daysSinceStart as Number, duration as Number, maxDuration as Number)
    {
        var bottom = canvas.y + canvas.height - Graphics.getFontHeight(Graphics.FONT_GLANCE);
        var fullHeight = bottom - canvas.y - margin;
        var height = ((duration.toFloat() / maxDuration.toFloat()) * fullHeight).toNumber();
        
        dc.fillRectangle(canvas.x + margin + barPadding + daysSinceStart * (barWidth + barPadding * 2), bottom - height, barWidth, height);
    } 
}