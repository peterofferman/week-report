import Toybox.Graphics;
import Toybox.Lang;
using Toybox.Time;

class LineChart extends Chart {
    function initialize(canvas as Rectangle, barWidth as Number, padding as Number, margin as Number) {
        Chart.initialize(canvas, barWidth, padding, margin);
    }

    function draw(dc as Dc, firstDay as Time.Moment, chartPoints as Array<ChartPoint>) as Void {
        var maxDuration = MathHelper.max(Time.Gregorian.SECONDS_PER_HOUR, getMaxValue(chartPoints));
        var maxDurationText = (maxDuration / Time.Gregorian.SECONDS_PER_HOUR).format("%2d");    
        
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        drawLegend(dc, maxDurationText + WatchUi.loadResource(Rez.Strings.HoursShort).toString(), maxDuration, maxDuration);
        drawAxes(dc);

        var previousDaysDiff = -1;
        var previousHeight = -1;
        
        var bottom = canvas.y + canvas.height - Graphics.getFontHeight(Graphics.FONT_GLANCE);
        var fullHeight = bottom - canvas.y - margin;
        var fullDayWidth = (barWidth + barPadding * 2);
        
        for(var i = 0; i < chartPoints.size(); i++)
        {
            var daysDiff = chartPoints[i].daysSinceStart;
            var previousX = canvas.x + margin + previousDaysDiff * fullDayWidth;
            var x = canvas.x + margin + daysDiff * fullDayWidth;
            var height = bottom - ((chartPoints[i].seconds.toFloat() / maxDuration.toFloat()) * fullHeight);
            
            if(previousHeight >= 0)
            {
                dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
                dc.drawLine(previousX, previousHeight, x, height);
            }            
            previousDaysDiff = daysDiff;
            previousHeight = height;
        }
        
        for(var i = 0; i < chartPoints.size(); i++)
        {
            var x = canvas.x + margin + chartPoints[i].daysSinceStart * fullDayWidth;
            var height = bottom - ((chartPoints[i].seconds.toFloat() / maxDuration.toFloat()) * fullHeight);
            dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
            dc.fillCircle(x, height, 4);
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
            dc.fillCircle(x, height, 2);            
        }

        for(var i = 0; i < 7; i++)
        {
            var dayInfo = Time.Gregorian.info(firstDay.add(Time.Gregorian.duration({:days => i})), Time.FORMAT_MEDIUM);
            drawDayText(dc, i, dayInfo.day_of_week.substring(0, 1));
        }

        // for(var i = 0; i < activityDurationPerDay.size(); i++)
        // {            
        //     var dayInfo = Gregorian.info(firstDay.add(Gregorian.duration({:days => i})), Time.FORMAT_MEDIUM);
        //     drawDayText(dc, chartSettings, i, dayInfo.day_of_week.substring(0, 1));
        //     if(maxDuration > 0)
        //     {
        //        drawDurationBar(dc, chartSettings, i, activityDurationPerDay[i], MathHelper.max(maxDuration, labelMaxDuration));      
        //     }            
        // }
        //dc.drawRectangle(chartSettings.canvas.x, chartSettings.canvas.y, chartSettings.canvas.width, chartSettings.canvas.height);
    }
}