import Toybox.Graphics;
import Toybox.Lang;
using Toybox.Time; 

(:glance)
class Chart {
    var canvas as Rectangle;
    var barWidth as Number;
    var barPadding as Number;
    var margin as Number;

    function initialize(canvas as Rectangle, barWidth as Number, padding as Number, margin as Number)
    {
        self.canvas = canvas;
        self.barWidth = barWidth;
        self.barPadding = padding;
        self.margin = margin;
    }

    function draw(dc as Dc, firstDay as Time.Moment, data as Array)
    {
        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_GLANCE, "Not Implemented", Graphics.TEXT_JUSTIFY_CENTER);
    }

    function drawDayText(dc as Dc, index as Number, text as String)
    {
        var startX = canvas.x + margin + barPadding + (barWidth / 2);
        var xOffsetPerDay = barWidth + (barPadding * 2);
        dc.drawText(
            startX + index * xOffsetPerDay,
            canvas.y + canvas.height - Graphics.getFontHeight(Graphics.FONT_GLANCE),
            Graphics.FONT_GLANCE, text, Graphics.TEXT_JUSTIFY_CENTER);
    }
    
    function drawLegend(dc as Dc, maxDurationText as String, maxDuration as Numeric, labelMaxDuration as Numeric)
    {
        var fullDayWidth = (barWidth + 2 * barPadding);
        dc.drawText(
            canvas.x + margin + 7 * fullDayWidth + margin,
            canvas.y,
            Graphics.FONT_GLANCE, maxDurationText, Graphics.TEXT_JUSTIFY_LEFT);

        var bottom = canvas.y + canvas.height - Graphics.getFontHeight(Graphics.FONT_GLANCE);
        var fullHeight = bottom - canvas.y - margin;
        var lineHeight = canvas.y + margin;
        if(maxDuration > labelMaxDuration)
        {
            lineHeight = bottom - ((fullHeight.toFloat() / maxDuration.toFloat()) * labelMaxDuration.toFloat()).toNumber();
        }

        dc.drawLine(canvas.x, lineHeight, canvas.x + margin + 7 * fullDayWidth + margin, lineHeight);
    }

    function drawAxes(dc as Dc)
    {
        var bottom = canvas.y + canvas.height - Graphics.getFontHeight(Graphics.FONT_GLANCE);
        var fullDayWidth = (barWidth + 2 * barPadding);
        dc.drawLine(canvas.x + margin, bottom, canvas.x + margin + fullDayWidth * 7 + 1, bottom);

        for(var i = 0; i < 8; i++)
        {
            var offset = fullDayWidth * i;
            dc.drawLine(canvas.x + margin + offset, bottom - 3, canvas.x + margin + offset, bottom);
        }
    }

    function getMaxValue(chartPoints as Array<ChartPoint>) as Number {
        var maxDuration = 0;
        for(var i = 0; i < chartPoints.size(); i++)
        {
            maxDuration = MathHelper.max(maxDuration, chartPoints[i].seconds);            
        }
        return maxDuration;
    }
}