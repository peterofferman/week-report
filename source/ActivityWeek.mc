import Toybox;
import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Time;
import Toybox.Lang;
using Toybox.Time;
using Toybox.Time.Gregorian;

(:glance)
class ActivityWeek {

    static function getFakeActivityDurationPerDay(firstDay as Time.Moment) as Array
    {
        return [new ChartPoint(0f, 22*60), new ChartPoint(1f, 5*60), new ChartPoint(2f, 35*60), new ChartPoint(3f, 10*60), 
                new ChartPoint(4f, 5*60), new ChartPoint(5f, 32*60), new ChartPoint(6f, 18*60)];
    }

    static function getActivityDurationPerDay(firstDay as Time.Moment) as Array<ChartPoint>
    {
        var activityDurationPerDay = [];
        for(var i = 0; i < 7; i++)
        {
            activityDurationPerDay.add(new ChartPoint(i.toFloat(),0));
        }
        var firstDayInfo = Gregorian.info(firstDay, Time.FORMAT_SHORT);     
        var activityIterator = UserProfile.getUserActivityHistory();
        if(activityIterator != null)
        {
            var sample = activityIterator.next();
            while(sample != null)
            {
                var startTime = convertGarminTimeToUnixTime(sample.startTime);
                var timeSinceFirstDay = startTime.compare(firstDay);
                if(timeSinceFirstDay > 0)
                {
                    var daysSinceFirstDay = (timeSinceFirstDay / Gregorian.SECONDS_PER_DAY).toNumber();
                    if(sample.duration != null)
                    {
                        var value = 0;
                        if(sample.duration instanceof Time.Duration)
                        {
                            value = (sample.duration as Time.Duration).value();
                        }else if(sample.duration instanceof Time.Moment)
                        {
                            value = (sample.duration as Time.Moment).value();
                        }else if(sample.duration instanceof Number || sample.duration instanceof Long)                        
                        {
                            value = sample.duration;
                        }
                        
                        if(daysSinceFirstDay < activityDurationPerDay.size())
                        {
                            activityDurationPerDay[daysSinceFirstDay].seconds += value; 
                        }
                    }               
                }
                sample = activityIterator.next();
            }
        }
        return activityDurationPerDay;
    }

    static function convertGarminTimeToUnixTime(input as Time.Moment)
    {
        var difference = Gregorian.duration({:years => 20, :days => -1});
        return input.add(difference);
    }
}