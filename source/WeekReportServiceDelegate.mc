import Toybox.Application;
import Toybox.Lang;
import Toybox.System;
using Toybox.Time;

(:background)
class WeekReportServiceDelegate extends ServiceDelegate 
{
    function initialize() {
        ServiceDelegate.initialize();                
    }

    function onWakeTime() {
        println("onWakeTime");
        FormatHelper.printCurrentTime();
        RecoveryTimes.update(false);
    }

    function onActivityCompleted(activity) {
        println("onActivityCompleted");
        FormatHelper.printCurrentTime();
        RecoveryTimes.updateOnActivity();
    }
}