import Toybox.Graphics;
import Toybox.WatchUi;
using Toybox.Time;
using Toybox.Time.Gregorian;
import Toybox.Lang;

(:glance)
class WeekReportGlanceView extends WatchUi.GlanceView {

    var chart;
    function initialize() {
        GlanceView.initialize();        
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        GlanceView.onLayout(dc);
        setupChart(dc);        
    }

    (:base)
    function setupChart(dc as Dc) as Void {
        chart = new BarChart(new Rectangle(0, 0, dc.getWidth(), dc.getHeight()), 12, 2, 4);
    }
    (:round)
    function setupChart(dc as Dc) as Void {
        chart = new BarChart(new Rectangle(0, 0, dc.getWidth(), dc.getHeight()), 12, 2, 4);
    }
    (:instinct2)
    function setupChart(dc as Dc) as Void {
        chart = new BarChart(new Rectangle(0, 0, dc.getWidth(), dc.getHeight()), 12, 2, 4);
    }
    (:instinct2s)
    function setupChart(dc as Dc) as Void {
        chart = new BarChart(new Rectangle(0, 4, dc.getWidth(), dc.getHeight() - 4), 10, 2, 4);
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        var firstDay = Time.today().subtract(Gregorian.duration({:days => 6}));
        var data = ActivityWeek.getActivityDurationPerDay(firstDay);
        chart.draw(dc, firstDay, data);
        //ActivityWeek.drawBarChart(dc, chartSettings);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }          
}
