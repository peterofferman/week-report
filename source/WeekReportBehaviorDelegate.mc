import Toybox.Application;
import Toybox.WatchUi;
class WeekReportBehaviorDelegate extends BehaviorDelegate {
    function initialize() {
        BehaviorDelegate.initialize();
    }
    
    function onNextPage() {
        System.println("test");
        WatchUi.pushView(new RecoveryTimeView(), new RecoveryTimeBehaviorDelegate(), WatchUi.SLIDE_UP);
        return true;
    }
}