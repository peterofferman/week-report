import Toybox.Graphics;
import Toybox.WatchUi;

class RecoveryTimeView extends WatchUi.View {

    var chart;
    var bmpSettings;
    
    function initialize() {
        View.initialize();        
    }

    // Load your resources here
    function onLayout(dc as Dc) as Void {
        View.onLayout(dc);
        setLayout(Rez.Layouts.RecoveryTimeLayout(dc));        
        setup(dc);
        //bmpSettings = new BitmapSettings(WatchUi.loadResource(Rez.Drawables.LauncherIcon), 0, 0);
    }

    (:base)
    function setup(dc as Dc) as Void {
        var xMargin = 9;
        chart = new LineChart(new Rectangle(xMargin, 77, dc.getWidth() - xMargin * 2, 62), 16, 0, 4);
    }
    (:round)
    function setup(dc as Dc) as Void {
        chart = new LineChart(new Rectangle(dc.getWidth() * 0.125, dc.getHeight() * 0.25, dc.getWidth() * 0.75, dc.getHeight() * 0.5), 16, 0, 4);
    }
    (:instinct2)
    function setup(dc as Dc) as Void {
        var xMargin = 9;
        chart = new LineChart(new Rectangle(xMargin, 77, dc.getWidth() - xMargin * 2, 62), 16, 0, 4);
        bmpSettings = new BitmapSettings(WatchUi.loadResource(Rez.Drawables.LauncherIcon), 113, 0);
    }
    (:instinct2s)
    function setup(dc as Dc) as Void {
        var xMargin = 9;
        chart = new LineChart(new Rectangle(xMargin, 62, dc.getWidth() - xMargin * 2, 62 - 4), 14, 0, 4);
        bmpSettings = new BitmapSettings(WatchUi.loadResource(Rez.Drawables.LauncherIcon), 0, 0);
    }


    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);  
        var firstDay = Time.today().subtract(Time.Gregorian.duration({:days => 6}));
        var chartPoints = RecoveryTimes.getChartPoints(firstDay);
        chart.draw(dc, firstDay, chartPoints);
        if(bmpSettings != null)
        {
            dc.drawBitmap(bmpSettings.x, bmpSettings.y, bmpSettings.bitmap);
        }
    }        

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

}
