import Toybox.Lang;

(:glance)
class Rectangle{
    var x as Number;
    var y as Number;
    var width as Number;
    var height as Number;

    function initialize(x, y, width, height)
    {
        self.x = x;
        self.y = y;
        self.width = width;
        self.height = height;
    }
}