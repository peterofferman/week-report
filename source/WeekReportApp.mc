import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
using Toybox.System;

(:background)
class WeekReportApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize(); 
        // Background.deleteActivityCompletedEvent();
        // Background.deleteTemporalEvent();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {     
        FormatHelper.printCurrentTime();   
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        init();
        return [ new WeekReportView(), new WeekReportBehaviorDelegate() ] as Array<Views or InputDelegates>;
    }

    function getGlanceView() {
        init();
        return [ new WeekReportGlanceView()];        
    }    

    function getServiceDelegate() as Array<System.ServiceDelegate> {
        return [new WeekReportServiceDelegate()];
    }

    function init() as Void {
        System.println("WeekReportApp.init()");
        //RecoveryTimes.update();
        RecoveryTimes.init();
        if(!Background.getActivityCompletedEventRegistered())
        {
            System.println("register for events");
            Background.registerForActivityCompletedEvent();
            Background.registerForWakeEvent();
        }
    }
}

function getApp() as WeekReportApp {
    return Application.getApp() as WeekReportApp;
}