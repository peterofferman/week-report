import Toybox.Application;
import Toybox.Graphics;
import Toybox.Lang;
import Toybox.WatchUi;
using Toybox.ActivityMonitor;
using Toybox.System;
using Toybox.Time;
using Toybox.Math;


(:background)
class RecoveryTimes {
    static var recoveryTimes as Array<Dictionary>?;
    static var lastRecoveryTime as Number?;
    static function init()
    {
        System.println("RecoveryTimes.init()");
        if(recoveryTimes == null)
        {
            if(Storage.getValue("recoveryTimes") == null)
            {
                recoveryTimes = [];
                lastRecoveryTime = null;
                saveValues();
            }else
            {
                loadValues();
            }
        }        
    }

    static function updateOnActivity() as Void {
        System.println("RecoveryTimes.updateOnActivity()");
        init();
        if(lastRecoveryTime != null)
        {
            var prevTimestamp = recoveryTimes[recoveryTimes.size() -1]["timestamp"].toNumber();
            var timestamp = Time.now().value();
            var hourDiff = (timestamp - prevTimestamp).toFloat() / Time.Gregorian.SECONDS_PER_HOUR;
            System.println("prevTimestamp:");
            System.println(prevTimestamp);
            System.println("timestamp:");
            System.println(timestamp);
            System.println("hourDiff:");
            System.println(hourDiff);
            var newRecoveryTime = MathHelper.max(0, Math.round(lastRecoveryTime - hourDiff));
            if(newRecoveryTime == 0 && lastRecoveryTime > 0)
            {
                System.println("crossing zero:");
                var zeroCrossTimestamp = prevTimestamp + Time.Gregorian.duration({:hours => lastRecoveryTime}).value();
                System.println(zeroCrossTimestamp);                
                recoveryTimes.add({"timestamp" => zeroCrossTimestamp, "recoveryTime" => 0});    
            }
            System.println(newRecoveryTime);
            recoveryTimes.add({"timestamp" => timestamp, "recoveryTime" => newRecoveryTime});
            saveValues();
            update(true);
        }
    }

    static function update(forceSave as Boolean?)
    {
        System.println("RecoveryTimes.update()");
        var saveNeeded = false;
        init();
        var newRecoveryTime = ActivityMonitor.getInfo().timeToRecovery;
        System.println(Time.now().value());
        System.println(lastRecoveryTime);
        System.println(newRecoveryTime);        
        if(newRecoveryTime != null && newRecoveryTime != lastRecoveryTime)
        {
            saveNeeded = true;
            if(newRecoveryTime == 0 && lastRecoveryTime != null && lastRecoveryTime > 0)
            {
                System.println("crossing zero:");
                var prevTimestamp = recoveryTimes[recoveryTimes.size() -1]["timestamp"].toNumber();
                var zeroCrossTimestamp = prevTimestamp + Time.Gregorian.duration({:hours => lastRecoveryTime}).value();
                System.println(zeroCrossTimestamp);                
                recoveryTimes.add({"timestamp" => zeroCrossTimestamp, "recoveryTime" => 0});    
            }
            lastRecoveryTime = newRecoveryTime;
            recoveryTimes.add({"timestamp" => Time.now().value(), "recoveryTime" => newRecoveryTime});            
        }
        var cutoffMoment = Time.today().subtract(Time.Gregorian.duration({:days => 6})).value().toNumber();
        for(var i = recoveryTimes.size() - 1; i >= 0; i--)
        {
            System.println(i)   ;
            System.println(recoveryTimes[i]);
            System.println(recoveryTimes[i]["timestamp"]);
            System.println(cutoffMoment);
            if(recoveryTimes[i]["timestamp"].toNumber() < cutoffMoment)
            {
                saveNeeded = true;
                recoveryTimes.remove(recoveryTimes[i]);
            }
        }

        if(saveNeeded || forceSave)
        {
            saveValues();
        }        
    }    
    static function saveValues()
    {
        System.println("saveValues()");
        Storage.setValue("recoveryTimes", recoveryTimes);
        Storage.setValue("lastRecoveryTime", lastRecoveryTime);
    }

    static function loadValues()
    {
        System.println("loadValues()");
        recoveryTimes = Storage.getValue("recoveryTimes");
        lastRecoveryTime = Storage.getValue("lastRecoveryTime");
    }

    static function getChartPoints(startMoment as Time.Moment) as Array<ChartPoint>
    {
        var startTimestamp = startMoment.value();
        var chartPoints = [];
        for(var i = 0; i < recoveryTimes.size(); i++)
        {
            chartPoints.add(new ChartPoint((recoveryTimes[i]["timestamp"].toNumber() - startTimestamp).toFloat() / Time.Gregorian.SECONDS_PER_DAY.toFloat(), 
                recoveryTimes[i]["recoveryTime"] * Time.Gregorian.SECONDS_PER_HOUR));
        }
        
        var currentRecoveryTime = ActivityMonitor.getInfo().timeToRecovery;
        if(currentRecoveryTime != null)
        {
            chartPoints.add(new ChartPoint((Time.now().value() - startTimestamp).toFloat()  / Time.Gregorian.SECONDS_PER_DAY.toFloat(), 
                currentRecoveryTime * Time.Gregorian.SECONDS_PER_HOUR));
        }
        return chartPoints;
    }    
}