# Week Report

This widget shows a bar chart of your activity duration over the past week at a glance.

I plan to implement a line chart for the recovery time as well as some more detailed views, so stay tuned for updates!